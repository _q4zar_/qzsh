from uuid import uuid4

class fnx:

    def gaa(self):
        cm=input("`> commit_message: ")
        bn=input("`> branch_name: ")
        return {
            1: f'git add --all',
            2: f'git commit -am "{cm}"',
            3: f'git push origin {bn}',
        }

    def gfa(self):
        return {
            1: f'git fetch --all',
        }

    def ga(self):
        f=input("`> files")
        cm=input("`> commit_message: ")
        bn=input("`> branch_name: ")
        return {
            1: f'git add {f}',
            2: f'git commit -m "{cm}',
            3: f'git push origin {bn}',
        }

    def gcnb(self):
        nbn=input("`> new_branch_name: ")
        return {
            1: f'git checkout -b {nbn}'
        }

    def archbootstrap(self):
        return {
            1: f"""
                pacman-key --init
                pacman-key --populate archlinux
                pacman -Syu
                pacman -S zsh docker docker-compose emacs"""
        }

    def create_ssh_4096(self):
        return {
            1: f'ssh-keygen -t rsa -b 4096'
        }
        
    def uid(self):
        _uid = str(uuid4()).replace('-','')
        return {
            1 : f'echo {_uid}'
        }