#!/bin/zsh
ZSH=("/home/qazar/.oh-my-zsh")
export ZSH
ZSH_THEME="clean"
plugins=(git)
source $ZSH/oh-my-zsh.sh
export LANG=en_US.UTF-8
export ARCHFLAGS="-arch x86_64"
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/q4ZAr/qazar.sh
path+=('/home/qazar/.yarn/bin')
path+=('/home/qazar/.local/bin')
export PATH