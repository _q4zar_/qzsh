export ZSH="/home/$USER/.oh-my-zsh"

ZSH_THEME="clean"

plugins=(
    git
    zsh-autosuggestions
    fast-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh
source /home/$USER/.zsh_lib


export VISUAL="c"


export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
