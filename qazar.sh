alias qzr='python /home/qazar/q4ZAr/webx-gtfo/wbx/lib/p/p-tplt/qzr.py $@'
alias yri='rm -rf node_modules ; yarn install'
alias yrs='yarn run serve'
alias ydp='python /home/qazar/__Zaniah__/vue_build_deploy.py $1'

alias rf='rm -rf'
alias hg='history | grep'
rr(){
    echo "recursively removing every ${1} ${2}" 
    find . -iname $1  -exec rm -rf {} \;
    find . -iname $2  -exec rm -rf {} \;
}
alias dps='docker ps'
alias dub='sudo docker-compose up --build'
alias dubd='sudo docker-compose up --build -d'
alias dv='sudo docker-compose down'
alias ddv='sudo docker-compose down --volumes'
alias de='sudo docker exec -it ${1} ${2}'
alias gs='git status'
alias grso='git remote show origin'
rsyncdawg(){
    echo "1=source 2=user 3=host 4=destination"
    rsync --partial --progress --rsh=ssh ${1} ${2}@${3}:${4}
}
alias cho='sudo chown -R qazar:qazar *'
export GOPATH=$(go env GOPATH)
export PATH=$PATH:$(go env GOPATH)/bin
dkrmall(){
    docker system prune -a --volumes
}
rsy(){
    rsync -azP ${1} ${2}
}
create_ssh_key(){
    ssh-keygen -t rsa -b 4096 -C 
}
