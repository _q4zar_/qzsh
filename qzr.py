
import subprocess
import inspect
import sys

class x:
    def __init__(self, obj):
        self.obj = obj
        self.args = sys.argv

    def run(self):
        self.fnx = inspect.getmembers(self, predicate=inspect.ismethod)
        for f in self.fnx:
            if f[0][:2] == '_a':
                f[1]()

    def _a_quasar_macros(self):
        fnx = getattr(self.obj.fnx, self.args[1])
        print(f'{fnx}')
        for key , val in fnx().items():
            print(f'{key} {val}')
            out = subprocess.run(val, shell=True, check=True)
            print(f'{out.stdout}')

from fnx import fnx
class obj:
    def __init__(self):
        self.fnx = fnx()
x(obj()).run()